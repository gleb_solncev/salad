<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function(){

    Route::group([
        'namespace' => 'Backend',
        'prefix' => 'admin',
        'middleware' => ['auth'],
        'as' => 'backend.',
    ], function () {
        include_once 'backend.php';
    });


    Route::group([
        'as' => 'frontend.', 'namespace' => 'Frontend',
    ], function () {
        include_once 'frontend.php';
    });
});
<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 02.01.2020 17:29
 *
 * Маршрутизация для фронта.
 */

use App\Models\Page;
//use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::post('send_message', 'PageController@message')->name('send_message');

Route::get('setlocale/{lang}', 'LocaleController@setLocale')->name('setlocale');

# Single Page
Route::get('', 'PageController@home')->name('page.home');
Route::get('{slug}', 'PageController@page')->name('page');



# Archive
Route::get('{post_type}/{slug}', 'PageController@single')->name('single');
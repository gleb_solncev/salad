<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 02.01.2020 17:30
 *
 * ${PARAM_DOC}
 * ${THROWS_DOC}
 */
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'projects'], function(){
    Route::post('/restore', 'ProjectController@restore')->name('projects.restore');
    Route::get('trash', 'ProjectController@deleted')->name('projects.trash');

    Route::resource('', 'ProjectController')
        ->except(['show'])->parameters([
            '' => 'project'
        ])->names('projects');
});

Route::get('home', 'HomeController@index')->name('home');

Route::resource('pages', 'PageController')
    ->except(['show', 'create', 'store'])
    ->names('pages');

Route::resource('media', 'MediaController')
    ->except(['show','create','show','edit','update'])
    ->names('media');
Route::get('get_images', 'MediaController@getImages')->name('media.ajax_get_images');


Route::resource('theme/{slug}/option', 'ThemeOptionController')
    ->only(['index', 'update'])
    ->names('theme_option');



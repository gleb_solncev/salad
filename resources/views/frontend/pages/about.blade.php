@extends('frontend.layouts.pages')

@section('title')
    {{ $fields->get('post_title') }}
@endsection

@section('app.css')
    @if(file_exists(getcwd().DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.str_replace('#', '', $fields->get('logo_color')).'.css'))
        <link rel="stylesheet" href="{{ asset('/css/'.(str_replace('#', '', $fields->get('logo_color'))?:'app').'.css') }}"/>
    @else
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
    @endif

@endsection

@section('content')
    <!--MAIN start-->
    <main class="main">
        <section class="about">
            <div class="site-width">
                <div class="container-fluid">
                    <h2 class="about__title">
                        {{ $fields->get('about__title') }}
                        <span class="bordered">{{ $fields->get('bordered') }}</span>
                    </h2>
                    <p class="about__desc">{{ $fields->get('about__desc') }}</p>
                    <a href="" class="btn about__btn btn-modal">{{ __('page.submitIt') }}</a>
                </div>
            </div>
        </section>
    </main>
    <!--MAIN end-->
@endsection

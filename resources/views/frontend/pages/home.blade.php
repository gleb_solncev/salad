@extends('frontend.layouts.pages')

@section('title')
    {{ $fields->get('post_title') }}
@endsection

@section('app.css')
    @if(file_exists(getcwd().DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.str_replace('#', '', $fields->get('logo_color')).'.css'))
        <link rel="stylesheet" href="{{ asset('/css/'.(str_replace('#', '', $fields->get('logo_color'))?:'app').'.css') }}"/>
    @else
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
    @endif

@endsection

@section('content')
    <!--MAIN start-->
    <main class="main">

        <section class="hero">
            <div class="site-width">
                @if($fields->get('video')->image)
                    <video id="video1" class="hero__video lazy_video" autoplay loop muted>
                        <data-src src="{{ $fields->get('video')->image->link }}" type="video/mp4"></data-src>
                        Your browser does not support HTML5 video.
                    </video>
                @endif
                <div class="shadow"></div>
                <button class="btn btn-modal" id="heroBtn">
                    <span class="btn__word">{{ __('page.dev') }}</span>
                    <span class="btn__word">{{ __('page.upak') }}</span>
                    <span class="btn__word">{{ __('page.reklama') }}</span>
                </button>
                <marquee class="hero__words" scrollamount="40">
                    <span class="hero__item">{{ $fields->get('marquee') }}</span>
                    <span class="hero__item">{{ $fields->get('marquee') }}</span>
                    <span class="hero__item">{{ $fields->get('marquee') }}</span>
                    <span class="hero__item">{{ $fields->get('marquee') }}</span>
                </marquee>
                <svg class="icon icon-mouse">
                    <use xlink:href="assets/svg/sprite.svg#mouse"/>
                </svg>
            </div>

        </section>

        <section class="cases">
            <div class="site-width">
                <div class="container-fluid">

                    <div class="row cases__row">
                        @if($fields->get('video_1')->image)
                            <a href="item.html" class="cases__item">
                                <video class="lazy_video" autoplay loop muted>
                                    <data-src src="{{ $fields->get('video_1')->image->link }}"
                                              type="video/mp4"></data-src>
                                    Your browser does not support HTML5 video.
                                </video>
                            </a>
                        @endif
                        @if($fields->get('video_2')->image)
                            <a href="item.html" class="cases__item">
                                <video class="lazy_video" autoplay loop muted>
                                    <data-src src="{{ $fields->get('video_2')->image->link }}"
                                              type="video/mp4"></data-src>
                                    Your browser does not support HTML5 video.
                                </video>
                            </a>
                        @endif
                        @if($fields->get('video_3')->image)
                            <a href="item.html" class="cases__item">
                                <video class="lazy_video" autoplay loop muted>
                                    <data-src src="{{ $fields->get('video_3')->image->link }}"
                                              type="video/mp4"></data-src>
                                    Your browser does not support HTML5 video.
                                </video>
                            </a>
                        @endif
                        @if($fields->get('video_4')->image)
                            <a href="item.html" class="cases__item">
                                <video class="lazy_video" autoplay loop muted>
                                    <data-src src="{{ $fields->get('video_4')->image->link }}"
                                              type="video/mp4"></data-src>
                                    Your browser does not support HTML5 video.
                                </video>
                            </a>
                        @endif
                        @if($fields->get('video_5')->image)
                            <a href="item.html" class="cases__item">
                                <video class="lazy_video" autoplay loop muted>
                                    <data-src src="{{ $fields->get('video_5')->image->link }}"
                                              type="video/mp4"></data-src>
                                    Your browser does not support HTML5 video.
                                </video>
                            </a>
                        @endif
                    </div>

                </div>
            </div>
        </section>

    </main>
    <!--MAIN end-->
@endsection

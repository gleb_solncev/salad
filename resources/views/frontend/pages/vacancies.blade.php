@extends('frontend.layouts.pages')

@section('title')
    {{ $fields->get('post_title') }}
@endsection

@section('app.css')
    @if(file_exists(getcwd().DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.str_replace('#', '', $fields->get('logo_color')).'.css'))
        <link rel="stylesheet" href="{{ asset('/css/'.(str_replace('#', '', $fields->get('logo_color'))?:'app').'.css') }}"/>
    @else
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
    @endif

@endsection

@section('content')
    <!--MAIN start-->
    <main class="main">

        <section class="vacancies">
            <div class="site-width">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col vacancies__info">
                            <h2 class="vacancies__title">{{ $fields->get('vacancies__title') }}
                                <span class="bordered">{{ $fields->get('vacancies__bordered') }}</span>
                            </h2>
                            <p class="vacancies__desc">
                                {{ $fields->get('vacancies__desc') }}
                            </p>
                        </div>
                        <div class="col vacancies__img">
                            @if($fields->get('vacancies__img')->image)
                                @if($fields->get('vacancies__img')->image->type == 'image')
                                    <img src="{{ $fields->get('vacancies__img')->image->link }}" alt="">
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <ul class="vacancies__list">
                            <li class="vacancies__item"><a href="{{ $fields->get('vacancies__item_href_1') }}" class="vacancies__link">{{ $fields->get('vacancies__item_title_1') }}</a></li>
                            <li class="vacancies__item"><a href="{{ $fields->get('vacancies__item_href_2') }}" class="vacancies__link">{{ $fields->get('vacancies__item_title_2') }}</a></li>
                            <li class="vacancies__item"><a href="{{ $fields->get('vacancies__item_href_3') }}" class="vacancies__link">{{ $fields->get('vacancies__item_title_3') }}</a></li>
                            <li class="vacancies__item"><a href="{{ $fields->get('vacancies__item_href_4') }}" class="vacancies__link">{{ $fields->get('vacancies__item_title_4') }}</a></li>
                            <li class="vacancies__item"><a href="{{ $fields->get('vacancies__item_href_5') }}" class="vacancies__link">{{ $fields->get('vacancies__item_title_5') }}</a></li>
                            <li class="vacancies__item"><a href="{{ $fields->get('vacancies__item_href_6') }}" class="vacancies__link">{{ $fields->get('vacancies__item_title_6') }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <!--MAIN end-->
@endsection

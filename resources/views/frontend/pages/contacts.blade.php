@extends('frontend.layouts.pages')

@section('title')
    {{ $fields->get('post_title') }}
@endsection

@section('app.css')
    @if(file_exists(getcwd().DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.str_replace('#', '', $fields->get('logo_color')).'.css'))
        <link rel="stylesheet" href="{{ asset('/css/'.(str_replace('#', '', $fields->get('logo_color'))?:'app').'.css') }}"/>
    @else
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
    @endif

@endsection

@section('content')
    <!--MAIN start-->
    <main class="main">
        <section class="contacts">
            <div class="site-width">
                <div class="container-fluid">
                    <div class="row contacts__row">
                        <div class="col contacts__title">
                            <h2>
                                {{ $fields->get('contacts__title') }}
                                <span class="bordered">{{ $fields->get('contacts__bordered') }}</span>
                            </h2>
                        </div>
                        <div class="col contacts__info">
                            <div class="row contacts__links" style="flex-wrap: wrap">
                                <div class="col contacts__col">
                                    <h4 class="contacts__subtitle">{{ __('page.contacts_subtitle') }}</h4>
                                    <a href="tel:{{ str_replace([' ', ')', '('], '', $fields->get('contacts__phone_1'))  }}" class="contacts__link">{{ $fields->get('contacts__phone_1')  }}</a>
                                    <a href="tel:{{ str_replace([' ', ')', '('], '', $fields->get('contacts__phone_2'))  }}" class="contacts__link">{{ $fields->get('contacts__phone_2')  }}</a>
                                </div>
                                <div class="col contacts__col">
                                    <h4 class="contacts__subtitle">{{ __('page.email') }}</h4>
                                    <a href="" class="contacts__link">{{ $fields->get('contacts__email_1')  }}</a>
                                    <a href="" class="contacts__link">{{ $fields->get('contacts__email_2')  }}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col contacts__img">
                            @if($fields->get('contacts__img')->image)
                                <img src="{{ $fields->get('contacts__img')->image->link }}" alt="">
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="contacts__map">
                            <img src="/assets/img/jpg/map.jpg" alt="">
                            <div class="point">
                                <span class="point__name">Salad</span>
                                <svg class="icon icon-point">
                                    <use xlink:href="assets/svg/sprite.svg#point"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <marquee class="hero__words contacts__words" scrollamount="80">
                        <span class="hero__item">{{ $fields->get('contacts__words') }}</span>
                        <span class="hero__item">{{ $fields->get('contacts__words') }}</span>
                        <span class="hero__item">{{ $fields->get('contacts__words') }}</span>
                        <span class="hero__item">{{ $fields->get('contacts__words') }}</span>
                    </marquee>

                </div>
            </div>
        </section>
    </main>
    <!--MAIN end-->
@endsection

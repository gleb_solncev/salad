@extends('frontend.layouts.pages')

@section('title')
    {{ $fields->get('post_title') }}
@endsection

@section('app.css')
    @if(file_exists(getcwd().DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.str_replace('#', '', $fields->get('logo_color')).'.css'))
        <link rel="stylesheet" href="{{ asset('/css/'.(str_replace('#', '', $fields->get('logo_color'))?:'app').'.css') }}"/>
        @else
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
    @endif

@endsection

@section('content')
    <!--MAIN start-->
    <main class="main">

        <section class="item">
            <div class="site-width">
                <div class="container-fluid">

                    @if($fields->get('thumbnail')->image)
                        <video id="video1" class="item__video lazy_video" autoplay loop muted controls>
                            <data-src src="{{ $fields->get('thumbnail')->image->link }}" type="video/mp4"></data-src>
                            Your browser does not support HTML5 video.
                        </video>
                    @endif

                    <marquee class="hero__words item__words" scrollamount="80" >
                            <span class="hero__item">{{ $fields->get('project_words') }}</span>
                            <span class="hero__item">{{ $fields->get('project_words') }}</span>
                            <span class="hero__item">{{ $fields->get('project_words') }}</span>
                            <span class="hero__item">{{ $fields->get('project_words') }}</span>
                    </marquee>
                </div>
                <div class="row">
                    <h2 class="item__loz">{{ $fields->get('project_title_1') }} <span
                                class="bordered">{{ $fields->get('project_title_2') }}</span>
                    </h2>
                </div>

                <div class="row tasks">
                    <span class="tasks__title">{{ __('page.tasks') }}:</span>
                    @if($fields->get('tasks__item_1'))
                        <span class="tasks__item">{{ $fields->get('tasks__item_1') }}</span>
                    @endif
                    @if($fields->get('tasks__item_2'))
                        <span class="tasks__item">{{ $fields->get('tasks__item_2') }}</span>
                    @endif
                    @if($fields->get('tasks__item_3'))
                        <span class="tasks__item">{{ $fields->get('tasks__item_3') }}</span>
                    @endif
                </div>

                <div class="row cases__row">
                    @if($fields->get('image_1')->image)
                        <div class="cases__item item__case">
                            <img src="{{ $fields->get('image_1')->image->link }}" alt="">
                        </div>
                    @endif
                    @if($fields->get('image_2')->image)
                        <div class="cases__item item__case">
                            <img src="{{ $fields->get('image_2')->image->link }}" alt="">
                        </div>
                    @endif
                    @if($fields->get('image_3')->image)
                        <div class="cases__item item__case">
                            <img src="{{ $fields->get('image_3')->image->link }}" alt="">
                        </div>
                    @endif
                </div>

                <div class="row item__info">
                    <div class="col">
                        <h2 class="item__title">{{$fields->get('single_title')}} <span
                                    class="bordered">{{$fields->get('title_2')}}</span></h2>
                        <p class="item__desc">
                            {!! $fields->get('single_desc') !!}
                        </p>
                    </div>
                    @if($fields->get('right_image')->image)
                        <div class="col item__logo">
                            <img src="{{ $fields->get('right_image')->image->link }}" alt="">
                        </div>
                    @endif
                </div>

                <div class="row item__images">
                    @if($fields->get('bottom_images_1')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_1')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_2')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_2')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_3')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_3')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_4')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_4')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_5')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_5')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_6')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_6')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_7')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_7')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_8')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_8')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_9')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_9')->image->link }}" alt=""></div>
                    @endif
                    @if($fields->get('bottom_images_10')->image)
                        <div class="img"><img src="{{ $fields->get('bottom_images_10')->image->link }}" alt=""></div>
                    @endif
                </div>

            </div>
        </section>
    </main>
    <!--MAIN end-->
@endsection


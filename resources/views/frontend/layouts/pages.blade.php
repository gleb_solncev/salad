@extends('frontend.html')

@section('body')
    <!--HEADER start-->
    <header class="header">
        <div class="site-width">
            <div class="container-fluid">
                <div class="row">

                    <a href="{{ route('frontend.page.home') }}" class="logo">salad</a>

                    <div class="burger" id="burger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>

                    <div class="header__right">
                        <div class="cross">
                            <span></span>
                            <span></span>
                        </div>
                        <nav class="navigation">
                            <ul class="nav">
                                <li class="nav__item">
                                    <a href="{{ route('frontend.page', 'projects') }}" class="nav__link">{{ __('page.projects') }}</a>
                                </li>
                                <li class="nav__item">
                                    <a href="{{ route('frontend.page', 'about') }}" class="nav__link">{{ __('page.about') }}</a>
                                </li>
                                <li class="nav__item">
                                    <a href="{{ route('frontend.page', 'vacancies') }}" class="nav__link">{{ __('page.vacancies') }}</a>
                                </li>
                                <li class="nav__item">
                                    <a href="{{ route('frontend.page', 'contacts') }}" class="nav__link">{{ __('page.contacts') }}</a>
                                </li>
                            </ul>
                        </nav>

                        <div class="lang__switch">
                            <span class="lang__active">{{ (app()->getLocale()=="uk"?"ua":app()->getLocale()) }}</span>
                            <ul class="lang__dropdown">
                                <li class="lang__item"><a href="{{ route('frontend.setlocale', 'ru') }}" class="lang__link">ru</a></li>
                                <li class="lang__item"><a href="{{ route('frontend.setlocale', 'uk') }}" class="lang__link">ua</a></li>
                                <li class="lang__item"><a href="{{ route('frontend.setlocale', 'en') }}" class="lang__link">en</a></li>
                            </ul>
                        </div>
                        <div class="phone__links">
                            <a href="" class="phone__link">WELCOME@DIGITAL.SALAD</a>
                            <a href="" class="phone__link">Instagram</a>
                            <a href="" class="phone__link">Facebook</a>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </header>
    <!--HEADER end-->

    @yield('content')

    @include('admin.includes.admin-menu')

@endsection
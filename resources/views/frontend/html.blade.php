<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"  @yield('attributes_to_html')>

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
{{--    <link href="{{ asset('assets/css/gleb.css') }}" rel="stylesheet">--}}

    @yield('app.css')

    <link rel="shortcut icon" type="image/png" href="{{ asset('/assets/img/png/favicon.ico') }}"/>

    <title>@yield('title')</title>
</head>

<body class="page" @yield('attributes_to_body')>

<div class="preloader">
    <span class="preloader__title">SALAD</span>
</div>

@yield('body')

<script src="{{ asset('assets/js/jquery-3.4.1.slim.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>


<!--MODALS start-->

<div class="pop-up" id="contactModal">
    <div class="pop-up__content">
        <div class="pop-up__cls">
            <span></span>
            <span></span>
        </div>
        <h3 class="pop-up__title">{{ __('page.contact_title') }} <span class="bordered">{{ __('page.contact_subtitle') }}</span></h3>
        <p class="pop-up__desc">{{ __('page.sub__title_contact') }}</p>
        <form action="{{ route('frontend.send_message') }}" method="POST" class="form pop-up__form" id="feedbackForm">
            @csrf
            <div class="form__row">
                <label for="name">{{ __('page.your_name') }}</label>
                <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="{{ __('page.ivan_ivanov') }}"
                        data-msg-minlength="Имя должно быть больше 4 символов"
                        data-msg-required="Обязательное поле"
                >
            </div>
            <div class="form__row">
                <label for="phone">{{ __('page.phone_number') }}</label>
                <input
                        type="tel"
                        name="phone"
                        id="phone"
                        placeholder="+38 (000) 0000-000"
                        data-msg-pattern="Не корректный формат номера телефона"
                        data-msg-required="Обязательное поле"
                        maxlength="13">
            </div>
            <div class="form__row">
                <button class="btn-add">{{ __('page.insert_message')  }}</button>
                <div class="msg-block">
                    <input type="text" name="mssg" id="mssg" placeholder="{{ __('page.message') }}">
                </div>
            </div>
            <div class="form__row">
                <input type="submit" value="{{ __('page.send') }}">
            </div>
        </form>
    </div>
</div>
<!--MODALS end-->

<!--FOOTER start-->
<footer class="footer">
    <div class="site-width">
        <div class="container-fluid">
            <div class="row footer__row">
                <a href="{{ route('frontend.page.home') }}" class="logo">salad</a>
                <a href="mailto:{{ TO::settings()->get('settings_email') }}" class="footer__link mail">{{ TO::settings()->get('settings_email') }}</a>
                <div class="footer__social">
                    <a href="{{ TO::settings()->get('settings_instagram_link') }}" class="footer__link">Instagram</a>
                    <a href="{{ TO::settings()->get('settings_facebook_link') }}" class="footer__link">Facebook</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--FOOTER end-->

<script src="/js/vendor.js"></script>
<script src="/js/app.js"></script>

</body>

</html>
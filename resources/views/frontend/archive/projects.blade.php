@extends('frontend.layouts.pages')



@section('app.css')
    @if(file_exists(getcwd().DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.str_replace('#', '', $fields->get('logo_color')).'.css'))
        <link rel="stylesheet" href="{{ asset('/css/'.(str_replace('#', '', $fields->get('logo_color'))?:'app').'.css') }}"/>
    @else
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
    @endif

@endsection

@section('content')

    <!--MAIN start-->
    <main class="main">

        <section class="cases projects">
            <div class="site-width">
                <div class="container-fluid">
                    <marquee
                            class="hero__words projects__words"
                            scrollamount="60"
                    >
                        <span class="hero__item">{{ $fields->get('projects__words') }}</span>
                        <span class="hero__item">{{ $fields->get('projects__words') }}</span>
                        <span class="hero__item">{{ $fields->get('projects__words') }}</span>
                        <span class="hero__item">{{ $fields->get('projects__words') }}</span>
                    </marquee>

                    <div class="row cases__row">
                        @foreach($list as $num => $item)
                            @if($item->get('thumbnail')->image)
                                <a href="{{ route('frontend.single', [$fields->get('slug'), $item->get('slug')]) }}" class="cases__item">
                                    <video class="lazy_video" autoplay loop muted>
                                        <data-src src="{{ $item->get('thumbnail')->image->link }}" type="video/mp4"></data-src>
                                        Your browser does not support HTML5 video.
                                    </video>
                                </a>
                            @endif
                        @endforeach
                    </div>

                </div>
            </div>
        </section>

    </main>
    <!--MAIN end-->
@endsection


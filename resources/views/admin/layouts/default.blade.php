@extends('admin.html')

@section('body')

<div class="container to-navigation">
    <a href="{{ url()->to('/') }}" class="logo" style="background-color: @yield('color-to-logo');">salad</a>
    {{--<ul style="display: inline-flex;justify-content: space-evenly;width: 50%;">
        <li><a href="{{ route('frontend.page', 'home') }}">{{ __('page.home') }}</a></li>
        <li><a href="{{ route('frontend.page', 'about') }}">{{ __('page.about') }}</a></li>
        <li><a href="{{ route('frontend.page', 'vacancies') }}">{{ __('page.vacancies') }}</a></li>
        <li><a href="{{ route('frontend.page', 'contacts') }}">{{ __('page.contacts') }}</a></li>
        <li><a href="{{ route('frontend.page', 'projects') }}">{{ __('page.projects') }}</a></li>
    </ul>--}}
    <ul class="d-inline-flex justify-content-end w-100 lipadding">
        <li>
            <a href="/setlocale/uk">Ukraine</a>
        </li>
        <li>
            <a href="/setlocale/ru">Russian</a>
        </li>
        <li>
            <a href="/setlocale/en">English</a>
        </li>
    </ul>
</div>

<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-header">Меню</div>

                <div class="card-body">
                    <ul class="gs_li">
                        <li>
                            <a href="{{ route('backend.home') }}">
                                Главная
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('backend.pages.index') }}">
                                Страницы
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('backend.media.index') }}">
                                Мультимедия
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('backend.projects.index') }}">
                                Проекты
                            </a>
                        </li>
                        @foreach(
                        App\Models\Page::where('type', 'theme_option')->get()
                        as $page
                        )
                            <li>
                                <a href="{{ route('backend.theme_option.index', $page->slug) }}">
                                    {{ $page->fields->where('name', 'title')->first()->value }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>
        <div class="col-9">
            @yield('content')
        </div>
    </div>
</div>
@endsection
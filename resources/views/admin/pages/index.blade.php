@extends('admin.layouts.default')

@section('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

@section('content')

    <div class="container">
        @if($pages)
            <div class="card">
                <div class="card-header">PAGES</div>
                <div class="card-body">
                    <table border="1" class="w-100">
                        <tr>
                            <th>#</th>
                            <th>Цвет</th>
                            <th>Заголовок</th>
                            <th>Дата</th>
                            <th>Дейсвия</th>
                        </tr>
                    @foreach($pages as $num => $page)
                        <tr>
                            <td>{{ ++$num }}</td>
                            <td>
                                <div class="box m-1"
                                     style="width:90%;text-align:center;height:auto;background-color: {{ $page->get('logo_color') }};color: white;"><b>SALAD</b></div>
                            </td>
                            <td>
                                {{ $page->get('title') }}
                            </td>
                            <td>
                                {{ $page->get('date') }}
                            </td>
                            <td>
                                <ul class="d-flex justify-content-start">
                                    <li class="p-2">
                                        <a class="btn btn-primary" href="{{ route('frontend.page', $page->get('slug')) }}">
                                            <i class="far fa-eye"></i>
                                        </a>
                                    </li>
                                    <li class="p-2">
                                        <a class="btn btn-primary" href="{{ route('backend.pages.edit', $page->get('slug')) }}">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        @else
            <div class="alert alert-danger">
                Страниц не существует! ОБратитесь к разработчику
            </div>
        @endif
    </div>
@endsection
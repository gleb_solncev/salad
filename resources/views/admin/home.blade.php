@extends('admin.layouts.default')

@section('content')
    <div class="card">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
            <div class="row mb-2">
                <div class="col-md-12">
                    <h1>Список сообщений</h1>
                </div>
            </div>

            <table>
                <tr>
                    <th>Имя</th>
                    <th>Номер телефона</th>
                    <th>Сообщение</th>
                    <th>Дата добавления</th>
                </tr>
                @foreach($messages as $message)
                    <tr style="line-height: 3">
                        <td style="width:20%">{{ $message->name }}</td>
                        <td style="width:20%">{{ $message->phone }}</td>
                        <td style="width:40%">{{ $message->message?:" <Пусто> " }}</td>
                        <td style="width:20%">{{ $message->created_at?$message->created_at->format('d.m.Y (H:i:s)'):'<TEST>' }}</td>
                    </tr>
                @endforeach
            </table>

            <div class="row mt-2">
                <div class="col-md-12">
                    {!! $messages->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

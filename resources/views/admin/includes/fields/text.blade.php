<div class="col-md-12">
    <div class="form-group">
        <label for="{{ $field->name }}">{{ $field->title }}</label>
        <input type="text"
               class="form-control"
               id="{{ $field->name }}" placeholder="{{ $field->title }}"
               value="{{ $field->value }}" name="{{ $field->name }}">
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label for="{{ $field->name }}">{{ $field->title }}</label>
        <textarea name="{{ $field->name }}" placeholder="{{ $field->title }}"
                  class="form-control" id="" cols="30" rows="10">{{ $field->value }}</textarea>
    </div>
</div>
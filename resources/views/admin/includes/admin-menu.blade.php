@auth
    <div class="admin-menu-block" style="z-index: 3">
        <div class="card">
            <div class="card-body">
                <ul>
                    <li>
                        <a style="z-index: 1; width: 60px; position: fixed; top: 85%; text-align: center;"
                           href="{{ route('backend.home') }}" class="btn btn-danger btn-block">
                            A
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endauth
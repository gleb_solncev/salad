@extends('admin.layouts.default')

@section('style')
    <style>
        .body-flat {
            border-radius: 0;
        }

        .toolbar {
            position: absolute;
            bottom: 70%;
        }

        .col-md-4 .box {
            border: 1px solid silver;
            padding: 10px;
            margin: 5px;
            max-height: 200px;
            min-height: 150px;
            background-image: url({{ asset('assets/img//load-gs.gif') }});
            background-position: center;
            text-align: center;
        }

        img, video {
            width: 100%;
            height: auto;
            max-height: 180px;
        }
    </style>
@endsection

@section('content')
    <div class="container">

        @if(session()->has('info'))
            <div class="alert alert-info">
                {!! session('info') !!}
            </div>
        @endif

        <div class="card body-flat">
            <div class="card-body">
                <form action="{{ route('backend.media.store') }}" method="POST" enctype="multipart/form-data"
                      class="d-flex justify-content-between">
                    @csrf
                    <input type="file" name="media[]" multiple>
                    <input class="btn btn-info btn-block" type="submit" value="Загрузить">
                </form>
            </div>
        </div>

        <div class="card body-flat">
            <div class="card-header">
                <div class="row d-flex justify-content-between">
                    <div class="title">MEDIA</div>
                    <div class="total">
                        Всего: {{ $media->total() }} шт.
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row justify-content-center mb-2">
                    {{ $media->links() }}
                </div>
                <div class="row">
                    @foreach($media as $mm)
                        <div class="col-md-4">
                            <div class="box">
                                @if($mm->type == "image")
                                    <img data-src="{{ asset('storage/'.$mm->filename) }}" alt="">
                                @elseif($mm['type'] == "video")
                                    <video data-src="{{$mm['link']}}" autoplay muted></video>
                                @endif
                                <div class="toolbar">
                                    <form action="{{ route('backend.media.destroy', $mm->id) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Удалить</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row justify-content-center">
                    {{ $media->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script>
        $(function () {
            var media = $('img, video');
            for (i = 0; i < media.length; i++) {
                $(media[i]).attr('src',
                    $(media[i]).data('src')
                );
                // $(media[i]).parent().css('background-image', "url("+$(media[i]).data('src')+")");
            }

        })
    </script>
@endsection
<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"  @yield('attributes_to_html')>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/gleb.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <title>@yield('title')</title>
    @yield('style')
</head>
<body style="background-color: @yield('color-BG')">

@yield('body')

<script src="{{ asset('assets/js/jscolor.js') }}"></script>
<script src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

@yield('scripts')

</body>
</html>
@extends('admin.layouts.default')

@section('color-to-logo') #227b14 @endsection
@section('color-BG') #227b14 @endsection

@section('style')
    <style>
        .select-image {
            width: 100%;
            height: auto;
            max-height: 100px;
            max-width: 100px;
            overflow: hidden;
        }

        .file-output, img, video {
            max-height: 100px;
            max-width: 100px;
            min-height: 100px;
            min-width: 100px;
            background-image: url('/assets/img/load-gs.gif');
            background-size: cover;
            background-position: center;
        }
    </style>
@endsection

@section('content')

    <div class="container">
        <?php if(session()->has('info')): ?>
            <div class="alert alert-success">
                <?php echo session('info'); ?>
            </div>
        <?php endif; ?>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <div class="title">PAGES</div>
                <div class="lang">{{ strtoupper(app()->getLocale()) }}</div>
            </div>
            <div class="card-body">
                <form action="{{ route('backend.projects.store') }}" method="POST">
                    @method('POST')
                    @csrf
                    <input type="hidden" name="parent_id" value="{{ $parent_id }}">
                    <div class="row d-flex justify-content-end">
                        <a class="btn btn-primary mr-2" href="{{ route('backend.projects.index') }}">Вернуться</a>
                        <button type="submit" class="btn btn-primary">Создать</button>
                    </div>

                    <div class="form-group">
                        <label>
                            SLUG
                            <input type="text" class="form-control" name="slug" value="{{ old('slug') }}">
                        </label>
                    </div>
                    @foreach($fields as $field)
                        @if($field->type == "text")
                            @include('admin.includes.fields.text')
                        @elseif($field->type == "color")
                            @include('admin.includes.fields.color')
                        @elseif($field->type == "media")
                            @include('admin.includes.fields.media')
                        @endif
                    @endforeach
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MEDIA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="ajax-imgs">
                        <div class="row"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        var html = $('html');
        $('button[data-toggle="modal"]').on('click', function (e) {
            e.preventDefault();
            var name = $(this).data('name'),
                ajax_mm = $('.ajax-imgs');

            ajax_mm.attr('data-name', name);
            ajax_mm.find('.row').html("Загрузка..");

            jQuery.get('{{ route('backend.media.ajax_get_images') }}', function (data) {
                ajax_mm.find('.row').html("");
                if (!data.length) $('.ajax-imgs').find('.row').html('Пусто! Добавьте мультимедию ');

                for (i = 0; i < data.length; i++) {
                    if (data[i].type === "image")
                        $('.ajax-imgs').find('.row').append(
                            '<div class="col-md-3"><img data-type="image" data-name="' + name + '" class="select-image" src="' + data[i].src + '" data-id="' + data[i].id + '"></div>'
                        );
                    else if (data[i].type === "video")
                        $('.ajax-imgs').find('.row').append(
                            '<div class="col-md-3"><video data-type="video" data-name="' + name + '" class="select-image" src="' + data[i].src + '" data-id="' + data[i].id + '" autoplay muted></video></div>'
                        );
                }
            });
            html = $('html');
        });

        html.on('click', '.select-image', function (e) {
            e.preventDefault();
            $('button.close').click();

            var name = $(this).data('name'),
                src = $(this).attr('src'),
                fileOutput = $('label[data-name="' + name + '"]').find('.file-output'),
                type = $(this).data('type');

            console.log(name, src);

            $('input[name="' + name + '"]').val($(this).data('id'));
            fileOutput.html('');
            if (type === "image")
                fileOutput.append('<img src="' + src + '" alt="' + name + '">');
            else if (type === "video")
                fileOutput.append('<video src="' + src + '" alt="' + name + '" muted autoplay></video>');
        });

        $('.clear-img').click(function (e) {
            e.preventDefault();
            var label = $(this).parents('label'),
                output = label.find('.file-output'),
                name = label.data('name'),
                src = "{{ asset('assets/img/add.png') }}";

            output.html('<img src="' + src + '" alt="' + name + '">');
            label.find('input[name="' + name + '"]').val('');
        });
    </script>
@endsection
<div class="col-md-12">
    <label data-name="{{ $field->name }}">
        {{ $field->title }}
        <p class="file-output">
            @if(optional($field->image)->type == "image")
                <img src="{{ asset('storage/'.$field->image->filename) }}"
                     alt="{{ $field->name }}">
            @elseif(optional($field->image)->type == "video")
                <video src="{{ asset('storage/'.$field->image->filename) }}"
                       alt="{{ $field->name }}" muted autoplay></video>
            @else
                <img src="{{ asset('assets/img/add.png') }}" alt="{{ $field->name }}">
            @endif
        </p>
        <p>
            <button class="btn btn-primary" data-toggle="modal"
                    data-target="#exampleModal"
                    data-name="{{ $field->name }}">
                Выбрать изображение
            </button>
            <button class="clear-img" data-name="{{$field->name}}">
                Очистить
            </button>
            <input type="hidden" name="{{ $field->name }}" value="{{ $field->value }}">
        </p>
    </label>
</div>
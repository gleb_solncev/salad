@extends('admin.layouts.default')

@section('content')

    <div class="container">

        <div class="container">
            <?php if(session()->has('info')): ?>
            <div class="alert alert-success">
                <?php echo session('info'); ?>
            </div>
            <?php endif; ?>


            @if($pages)
                <div class="card">
                    <div class="card-header">PROJECTS</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <a class="btn btn-primary mb-2" href="{{ route('backend.projects.create') }}">Добавить
                                проект</a>
                            <a class="btn btn-danger mb-2" href="{{ route('backend.projects.trash') }}">Удаленные
                                ({{ $trashed }})</a>
                        </div>

                        <table border="1" class="w-100">
                            <tr>
                                <th>#</th>
                                <th>Цвет</th>
                                <th>Заголовок</th>
                                <th>Дата</th>
                                <th>Дейсвия</th>
                            </tr>
                            @foreach($pages as $num => $page)
                                <tr>
                                    <td>{{ ++$num }}</td>
                                    <td>
                                        <div class="box m-1"
                                             style="width:90%;text-align:center;height:auto;background-color: {{ $page->get('logo_color') }};color: white;">
                                            <b>SALAD</b></div>
                                    </td>
                                    <td>
                                        {{ $page->get('title') }}
                                    </td>
                                    <td>
                                        {{ $page->get('date') }}
                                    </td>
                                    <td>
                                        <ul class="d-flex justify-content-start">
                                            <li class="p-2">
                                                <a class="btn btn-primary"
                                                   href="{{ route('frontend.single', [$page->get('parent_slug'), $page->get('slug')]) }}">
                                                    <i class="far fa-eye"></i>
                                                </a>
                                            </li>
                                            <li class="p-2">
                                                <a class="btn btn-primary"
                                                   href="{{ route('backend.projects.edit', $page->get('slug')) }}">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                            </li>
                                            <li class="p-2">
                                                <form action="{{ route('backend.projects.destroy', $page->get('slug')) }}"
                                                      method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger">Удалить</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            @else
                <div class="alert alert-danger">
                    Страниц не существует! ОБратитесь к разработчику
                </div>
            @endif
        </div>
@endsection
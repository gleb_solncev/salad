@extends('admin.layouts.default')



@section('content')

    <div class="container">
        @if($pages)
            <div class="card">
                <div class="card-header">PROJECTS</div>
                <div class="card-body">
                    <table border="1" class="w-100">
                        <tr>
                            <th>#</th>
                            <th>Цвет</th>
                            <th>Заголовок</th>
                            <th>Дата</th>
                            <th>Дейсвия</th>
                        </tr>
                        @foreach($pages as $num => $page)
                            <tr>
                                <td>{{ ++$num }}</td>
                                <td>
                                    @php($color = optional($page->fields->where('name', 'logo_color')->first())->value)
                                    <div class="box m-1"
                                         style="width:90%;text-align:center;height:auto;background-color: {{$color}};color: white;"><b>SALAD</b></div>
                                </td>
                                <td>
                                    {{ optional($page->fields->where('name', 'title')->first())->value }}
                                </td>
                                <td>
                                    @if($page->updated_at)
                                        {{ optional($page->updated_at)->format('d.m.y H:i') }}
                                    @else
                                        {{ optional($page->created_at)->format('d.m.y H:i') }}
                                    @endif
                                </td>
                                <td>
                                    <ul>
                                        <li>
                                            <form action="{{ route('backend.projects.restore') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="slug" value="{{ $page->slug }}">
                                                <button class="btn btn-primary btn-block" type="submit">Вернуть</button>
                                            </form>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        @else
            <div class="alert alert-danger">
                Страниц не существует! ОБратитесь к разработчику
            </div>
        @endif
    </div>
@endsection
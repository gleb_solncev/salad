<?php

return [

    'home' => 'Home',
    'about' => 'About',
    'vacancies' => 'Vacancies',
    'contacts' => 'Contacts',
    'projects' => 'Projects',
    'submitIt' => 'Click here',
    'contacts_subtitle' => 'Feedback numbers',
    'email' => 'Email',
    'tasks' => 'tasks',
    'dev' => 'development?',
    'upak' => 'packaging?',
    'reklama' => 'advertisement?',
    'contact_title' => 'CONTACT',
    'contact_subtitle' => ' US',
    'sub__title_contact' => 'WE WILL HELP TO UNDERSTAND AND ANSWER ALL QUESTIONS',
    'your_name' => 'YOUR NAME',
    'phone_number' => 'YOUR PHONE NUMBER',
    'message' => 'message',
    'ivan_ivanov' => 'IVAN IVANOV',
    'insert_message' => 'ADD MESSAGE',
    'send' => 'Send',
];

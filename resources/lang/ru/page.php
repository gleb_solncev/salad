<?php

return [

    'home' => 'Главная',
    'about' => 'О нас',
    'vacancies' => 'Вакансии',
    'contacts' => 'Контакты',
    'projects' => 'Проекты',
    'submitIt' => 'Нажми сюда',
    'contacts_subtitle' => 'Номер телефона',
    'email' => 'Почта',
    'contact_title' => 'Свяжитесь',
    'contact_subtitle' => 'с нами',
    'sub__title_contact' => 'Мы поможем разобраться и ответим на все вопросы',
    'your_name' => 'Ваше имя',
    'phone_number' => 'Ваш телефон',
    'message' => 'Сообщение',
    'ivan_ivanov' => 'Иван Иванов',
    'insert_message' => 'Добавить сообщение',
    'tasks' => 'задачи',
    'dev' => 'разработка?',
    'upak' => 'упаковка?',
    'reklama' => 'реклама?',
    'send' => 'Отправить',
];

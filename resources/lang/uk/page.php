<?php

return [

    'home' => 'Головна',
    'about' => 'О нас',
    'vacancies' => 'Вакансii',
    'contacts' => 'Контакти',
    'projects' => 'Проекти',
    'submitIt' => 'Натисни сюди',
    'contacts_subtitle' => 'Номери для зворотного зв`язку',
    'email' => 'Пошта',
    'tasks' => 'завдання',
    'dev' => 'розробка?',
    'upak' => 'упаковка?',
    'reklama' => 'реклама?',
    'send' => 'Відправити',

];

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function createnewCSS($logo_color): void
    {
        if($logo_color) {
            $format = '.css';
            $path = getcwd() . DIRECTORY_SEPARATOR . "css";
            $filename = str_replace('#', '', $logo_color);
            $content = file_get_contents($path . DIRECTORY_SEPARATOR . "app" . $format);

            $content = str_replace('#227b14', '#'.$filename, $content);

            $filename = $path . DIRECTORY_SEPARATOR . $filename . $format;
            fopen($filename, 'w');
            file_put_contents($filename, $content);
        }
    }
}

<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 03.01.2020 10:12
 *
 * Locale controller to setLocale
 */


namespace App\Http\Controllers\Frontend;


use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Http\RedirectResponse;

class LocaleController extends BaseController
{
    private $referer,
        $languages,
        $mainLanguage;

    public function __construct()
    {
        $this->referer = redirect()->back()->getTargetUrl();
        $this->languages = LocaleMiddleware::$languages;
        $this->mainLanguage = LocaleMiddleware::$mainLanguage;
    }


    /**
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setLocale($lang): RedirectResponse
    {
        # STEP 1 - Формирование URI
        $back_uri = str_replace([url('/')], '', $this->referer);

        # STEP 2 - Формируем метку на место смены языка
        $segments = array_filter(explode('/', $back_uri, 3));
        $locale = reset($segments); # LOCALE

        if (in_array($locale, $this->languages)){
            $key = array_search($locale, $segments); # Array KEY Integer
            unset($segments[$key]);
            sort($segments); // Для красоты
        }

        # STEP 3 - Формирование нового URL
        $url = implode( array_merge(
                array_filter([
                    url('/'),
                    $lang!=$this->mainLanguage?$lang:null,
                ]),
                $segments
            ), '/');


        return redirect()->to($url);
    }
}
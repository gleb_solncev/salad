<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 03.01.2020 10:12
 *
 * Locale controller to setLocale
 */


namespace App\Http\Controllers\Frontend;

use App\Models\Message;
use App\Services\TypeViewPostType\ArchiveViews;
use App\Services\TypeViewPostType\SingleViews;
use \App\Services\TypeViewPostType\PageViews;
use App\Services\TypeViewPostType\TypeView;
use App\Services\PageFields\ServicePageFields;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends BaseController
{
    /**
     * @var Page
     */
    private $page;

    /**
     * @var ServicePageFields
     */
    private $service;

    /**
     * PageController constructor.
     */
    public function __construct()
    {
        $this->page = new Page;
        $this->service = new ServicePageFields;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $page = $this->page
            ->where('isHomePage', true)
            ->firstOrFail();

        /** @var Page $page */
        return $this->page($page);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function page(Page $page)
    {
        if ($page->childs->isNotEmpty()) return $this->archive($page);
        $fields = $this->service->getFieldsByPage($page);# Получить поля данной страницы

        return view(TypeView::getView(new PageViews, $page->slug),
            compact('fields')
        );
    }

    /**
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function archive(Page $page)
    {
        $list = $this->service->getFieldsByCollection($page->childs);
        $fields = $this->service->getFieldsByPage($page);

        return view(TypeView::getView(new ArchiveViews, $page->slug),
            compact('fields', 'list')
        );
    }

    /**
     * @param $post_type
     * @param $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function single(Page $post_type, Page $page)
    {
        $fields = $this->service->getFieldsByPage($page);

        return view(TypeView::getView(new SingleViews, $post_type->slug),
            compact('fields', 'page', 'post_type')
        );
    }

    public function message(Request $request)
    {
        $message = new Message;

        $message->name = $request->name;
        $message->phone = $request->phone;
        $message->message = $request->mssg;

        $message->save();

        return response()->json(['status' => '201']);
    }

}




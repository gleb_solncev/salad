<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 03.01.2020 11:00
 *
 * Base controller extends by Controller
 * Abstract
 */


namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;

abstract class BaseController extends Controller
{

}
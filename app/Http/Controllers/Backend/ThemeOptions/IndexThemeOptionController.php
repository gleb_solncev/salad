<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 28.01.2020 11:03
 */


namespace App\Http\Controllers\Backend\ThemeOptions;


use App\Models\Page;

class IndexThemeOptionController extends AbstractThemeOptionController implements iStartThemeOptionProperty
{

    private $page;
    private $fields;
    private $collectionView;

    public function __construct(Page $page)
    {
        parent::__construct();
        $this->page = $page;
        $this->setFields();
        $this->setCollectionView();
    }

    public function start()
    {
        return view('admin.theme_option.edit', $this->collectionView);
    }

    private function setCollectionView(): void
    {
        $page = $this->page;
        $fields = $this->fields;

        $this->collectionView = compact('page', 'fields');
    }

    private function setFields(): void
    {
        $this->fields = $this->page->fields->reject(function ($page) {
            return in_array($page->name, ['post_title', 'title']);
        });
    }
}
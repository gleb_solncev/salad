<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 28.01.2020 11:03
 */


namespace App\Http\Controllers\Backend\ThemeOptions;


use App\Models\Page;
use Illuminate\Http\Request;

class UpdateThemeOptionController extends AbstractThemeOptionController implements iStartThemeOptionProperty
{

    private $page;
    private $fields;

    public function __construct(Page $page, Request $request)
    {
        parent::__construct();

        $this->page = $page;
        $this->fields = $request->except(['_token', '_method']);
        $this->setFields();
    }

    public function start()
    {
        return redirect()->back();
    }


    private function setFields(): void
    {
        collect($this->fields)->map(function ($value, $name) {
            $field = $this->page->fields->where('name', $name)->first();
            $field->value = $value;
            $field->save();
        });
    }
}
<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 28.01.2020 11:06
 */


namespace App\Http\Controllers\Backend\ThemeOptions;


class OpenEntityThemeOption
{
    public static function open(iStartThemeOptionProperty $type){
        return $type->start();
    }
}
<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 28.01.2020 11:05
 */


namespace App\Http\Controllers\Backend\ThemeOptions;


interface iStartThemeOptionProperty
{
    public function start();
}
<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Projects\
{
    TrashedProjectController,
    RestoreProjectController,
    DestroyProjectController,
    CreateProjectController,
    UpdateProjectController,
    StoreProjectController,
    IndexProjectController,
    EditProjectController,
    OpenEntityProject,
};
use App\Http\Requests\ProjectRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return OpenEntityProject::work(new IndexProjectController);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return OpenEntityProject::work(new CreateProjectController);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        return OpenEntityProject::work(new StoreProjectController($request));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return OpenEntityProject::work(new EditProjectController($page));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        return OpenEntityProject::work(new UpdateProjectController($request, $page));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        return OpenEntityProject::work(new DestroyProjectController($page));
    }

    /**
     * @return mixed
     */
    public function deleted()
    {
        return OpenEntityProject::work(new TrashedProjectController);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function restore(Request $request)
    {
        return OpenEntityProject::work(new RestoreProjectController($request->slug));
    }

}

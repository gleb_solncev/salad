<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * @var Media
     */
    private $model;

    public function __construct()
    {
        $this->model = new Media;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $media = $this->model
            ->orderBy('created_at', 'DESC')
            ->paginate(15);

        return view('admin.media.index',
            compact('media')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $media = $request->media;

        $mm = collect($media)->map(function ($file) {
            $file = $file->store('public');
            $stor_path = explode('/', $file);

            $filename = end($stor_path);
            $storage_path = storage_path('app\\public\\' . $filename);
            $link = asset("storage/$filename");
            $size = 1;//\File::size($storage_path);
            $type = explode('.', $filename);
            $format = end($type);
            $type = collect(config('filesystems.access_formats'))->map(function ($formats, $type) use ($format) {
                if (in_array($format, $formats))
                    return $type;
            })->filter()->first();

            return compact('link', 'storage_path', 'size', 'filename', 'type');
        });

        Media::insert($mm->toArray());

        return redirect()->back()
            ->with('info', "Число файлов: ".$mm->count().". Успешно загружено!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = $this->model->find($id);

        $path = storage_path('app\\public\\' . $media->filename);
        if (file_exists($path)) {
            $path_del = storage_path('app\\delete\\' . $media->filename);
            rename($path, $path_del);
        }

        $media->delete();

        return redirect()->back()
            ->with('info', 'Файл успешно удален. Для его восстановления обратитесь к разработчику');
    }


    public function getImages()
    {
        //dd(request()->type);
        $media = $this->model->orderBy('created_at', 'DESC')->get()->map(function($image){
            return [
                'id' => $image->id,
                'type' => $image->type,
                'src' => asset('storage/'.$image->filename),
            ];
        })->toArray();

        return response()->json($media);
    }
}

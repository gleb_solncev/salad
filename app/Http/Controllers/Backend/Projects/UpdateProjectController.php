<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 12:33
 */


namespace App\Http\Controllers\Backend\Projects;


use App\Models\PageFields;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Models\Page;

class UpdateProjectController extends AbstractProjectController implements iStartProjectProperty
{
    /**
     * @var Collection $fieldsToPage
     */
    private $fieldsToPage;

    /**
     * @var Collection $fielsToPageField
     */
    private $fielsToPageField;

    /**
     * @var Page $page
     */
    private $page;

    /**
     * UpdateProjectController constructor.
     *
     * @param Request $request
     * @param Page $page
     */
    public function __construct(Request $request, Page $page)
    {
        parent::__construct();

        $this->page = $page;

        $this->setFieldsToPage($request);
        $this->setFielsToPageField($request);

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function start()
    {
        $color = $this->fielsToPageField->get('logo_color');

        $this->updatePageFields();
        $this->updatePage();
        $this->createnewCSS($color);

        return redirect()->route('backend.projects.edit', $this->page->slug)->with('info', "Страница успешно обновлена.");
    }

    private function updatePage(): void
    {
        collect($this->fieldsToPage)->map(function (String $value, String $name) {
            $this->page->$name = $value;
        });
        $this->page->save();
    }

    private function updatePageFields(): void
    {
        $this->page->fields->map(function (PageFields $item) {
            $newValue = $this->fielsToPageField->get($item->name);
            $item->value = $newValue;
            $item->save();
        });
    }

    /**
     * @param Collection $fieldsToPage
     */
    private function setFieldsToPage(Request $request): void
    {
        $this->fieldsToPage = collect($request->only('slug', 'parent_id'));
    }

    /**
     * @param Collection $fielsToPageField
     */
    private function setFielsToPageField(Request $request): void
    {
        $this->fielsToPageField = collect($request->except(['_method', '_token', 'slug', 'parent_id']));
    }


}
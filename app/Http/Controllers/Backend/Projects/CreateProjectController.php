<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 12:33
 */


namespace App\Http\Controllers\Backend\Projects;


use Illuminate\Support\Collection;

class CreateProjectController extends AbstractProjectController implements iStartProjectProperty
{
    /**
     * @var int $parent_id
     */
    private $parent_id;

    /**
     * @var Collection $fields
     */
    private $fields;

    /**
     * @var array $collectionToView
     */
    private $collectionToView;

    public function __construct()
    {
        parent::__construct();
        $this->setParentId();
        $this->setFields();
        $this->setCollectionToView();
    }

    public function start()
    {
        return view('admin.projects.create', $this->collectionToView);
    }

    private function setParentId(): void
    {
        $demoPage = $this->getDemoPage();

        $this->parent_id = optional($demoPage)->parent_id;
    }

    private function setFields(): void
    {
        $this->fields = $this->getDemoDATA();
    }

    public function setCollectionToView(): void
    {
        $parent_id = $this->parent_id;
        $fields = $this->getDemoDATA();

        $this->collectionToView = compact('parent_id', 'fields');
    }
}
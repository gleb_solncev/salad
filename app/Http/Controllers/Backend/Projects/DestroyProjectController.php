<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 16:54
 */


namespace App\Http\Controllers\Backend\Projects;


use App\Models\Page;

class DestroyProjectController extends AbstractProjectController implements iStartProjectProperty
{
    /**
     * @var Page $page
     */
    private $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function start()
    {
        $this->page->delete();

        return redirect()
            ->route('backend.projects.index')
            ->with('info', 'Страница успешно удалена');
    }
}
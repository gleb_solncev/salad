<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 16:49
 */


namespace App\Http\Controllers\Backend\Projects;


use App\Models\Page;

class RestoreProjectController extends AbstractProjectController implements iStartProjectProperty
{
    /**
     * @var string $slug
     */
    private $slug;

    /**
     * @var Page $page
     */
    private $page;

    public function __construct(string $slug)
    {
        parent::__construct();

        $this->slug = $slug;

        $this->setPage();
    }

    public function start()
    {
        $this->restore();

        return redirect()->route('backend.projects.index')
            ->with('info', 'Страница успешно восстановлена');
    }

    public function setPage()
    {
        $this->page = $this->model->onlyTrashed()
            ->where('slug', $this->slug)
            ->first();
    }

    private function restore(){
        $this->page->restore();
    }
}
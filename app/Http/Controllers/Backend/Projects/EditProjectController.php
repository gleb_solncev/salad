<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 12:33
 */


namespace App\Http\Controllers\Backend\Projects;


use App\Models\Page;
use Illuminate\Support\Collection;

class EditProjectController extends AbstractProjectController implements iStartProjectProperty
{
    /**
     * @var Page $page
     */
    private $page;

    /**
     * @var Collection $fields
     */
    private $fields;

    /**
     * @var string $post_type
     */
    private $post_type;

    /**
     * @var array $collectionToView
     */
    private $collectionToView;

    /**
     * EditProjectController constructor.
     * @param $page
     */
    public function __construct(Page $page)
    {
        parent::__construct();

        $this->page = $page;

        $this->setFields();
        $this->setPostType();
        $this->setCollection();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function start()
    {
        return view('admin.projects.edit', $this->collectionToView);
    }

    private function setFields(): void
    {
        $this->fields = $this->page->fields;
    }

    private function setPostType(): void
    {
        $this->post_type = optional($this->page->parent)->slug;
    }

    public function setCollection(): void
    {
        $page = $this->page;
        $fields = $this->fields;
        $post_type = $this->post_type;

        $this->collectionToView = compact('page', 'fields', 'post_type');
    }
}
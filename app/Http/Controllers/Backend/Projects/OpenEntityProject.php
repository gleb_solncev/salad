<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 12:47
 */


namespace App\Http\Controllers\Backend\Projects;


class OpenEntityProject
{
    public static function work(iStartProjectProperty $start)
    {
        return $start->start();
    }
}
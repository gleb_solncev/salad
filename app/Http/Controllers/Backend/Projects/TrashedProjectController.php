<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 15:40
 */


namespace App\Http\Controllers\Backend\Projects;


use App\Models\Page;
use Illuminate\Support\Collection;

class TrashedProjectController extends AbstractProjectController implements iStartProjectProperty
{
    /**
     * @var Collection $pages
     */
    private $pages;

    /**
     * @var Page $parent
     */
    private $parent;

    /**
     * @var array $collectionView
     */
    private $collectionView;

    public function __construct()
    {
        parent::__construct();

        $this->setParent();
        $this->setPages();
        $this->setCollectionView();
    }


    public function start()
    {
        return view('admin.projects.deleted', $this->collectionView);
    }

    private function setPages(){
        $this->pages = Page::onlyTrashed()
            ->where('parent_id', optional($this->parent)->id)
            ->get();
    }

    /**
     * @param Page $parent
     */
    public function setParent(): void
    {
        $this->parent = $this->model->where('slug', 'projects')->first();
    }

    private function setCollectionView(){
        $pages = $this->pages;
        $parent = $this->parent;

        $this->collectionView = compact('pages', 'parent');
    }
}
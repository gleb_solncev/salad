<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 12:44
 */


namespace App\Http\Controllers\Backend\Projects;


interface iStartProjectProperty
{
    public function start();
}
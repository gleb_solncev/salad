<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 12:33
 */


namespace App\Http\Controllers\Backend\Projects;


use App\Http\Middleware\LocaleMiddleware;
use App\Models\Page;
use App\Models\PageFields;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class StoreProjectController extends AbstractProjectController implements iStartProjectProperty
{
    /**
     * @var Page $page
     */
    private $page;

    /**
     * @var Collection $fields
     */
    private $fields;

    /**
     * @var array $oldData
     */
    private $oldData;


    public function __construct(Request $request)
    {
        parent::__construct();

        $this->oldData = $request->except(['_method', '_token']);

        $this->savePage($request->only('slug', 'parent_id'));
        $this->setFields($request->except(['_method', '_token', 'parent_id', 'slug']));
    }

    public function start()
    {
        $this->saveFields();
        $this->createnewCSS($this->fields->where('name', 'logo_color')->first()->value);

        return redirect()->route('backend.projects.edit', $this->page->slug)
            ->with('info', 'Данная страница успешно создана');
    }

    /**
     * @param mixed $page
     */
    private function savePage(array $fillable): void
    {
        $page = Page::firstOrNew($fillable);
        $page->isActive = true;
        $page->isHomePage = false;

        $page->save();
        $this->page = $page;
    }

    private function setFields(array $data):void
    {
        $demoPage = Page::onlyTrashed()->where('parent_id', 5)->first();

        $this->fields = $demoPage->fields->map(function(PageFields $item) use($data){
            $item->value = collect($data)->get($item->name);
            $collection = [];

            foreach(LocaleMiddleware::$languages as $locale):
                $item->lang = $locale;
                $collection[] = new PageFields($item->toArray());;
            endforeach;

            return $collection;
        })->flatten(1);
    }


    private function saveFields(): void
    {
        $this->page->fields()->delete();
        $this->page->fields()->saveMany($this->fields);
    }
}
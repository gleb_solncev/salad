<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 12:37
 */


namespace App\Http\Controllers\Backend\Projects;


use App\Http\Controllers\Backend\BaseController;
use App\Models\Page;
use App\Services\PageFields\ServicePageFields;

abstract class AbstractProjectController extends BaseController
{
    /**
     * @var Page $model
     */
    protected $model;

    /**
     * @var ServicePageFields $service
     */
    protected $service;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Page;
        $this->service = new ServicePageFields;
    }

    public function getDemoPage()
    {
        return $this->model->onlyTrashed()->get()->where('parent_id', 5)->first();
        //$this->model->onlyTrashed->where('slug', 'projects')->first();
    }

    public function getDemoDATA()
    {
        $child = $this->getDemoPage();
        return $child->fields->map(function ($item) {
            $item->value = '';
            return $item;
        });
    }
}
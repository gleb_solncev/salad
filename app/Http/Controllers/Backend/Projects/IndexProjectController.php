<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 12:33
 */


namespace App\Http\Controllers\Backend\Projects;


use App\Models\Page;
use Illuminate\Support\Collection;

class IndexProjectController extends AbstractProjectController implements iStartProjectProperty
{

    /**
     * @var Collection $pages
     */
    private $pages;

    /**
     * @var Collection $trashed
     */
    private $trashed;

    /**
     * @var array $collectionView
     */
    private $collectionView;


    public function __construct()
    {
        parent::__construct();

        $this->setModal();
        $this->setPages();
        $this->setTrashed();
        $this->setCollectionView();
    }

    public function start()
    {
        return view('admin.projects.index', $this->collectionView);
    }

    private function setModal(): void
    {
        $this->model = $this->model->where('slug', 'projects')->first();
    }

    private function setPages(): void
    {
        $this->pages = optional($this->model)->childs;
    }

    private function setTrashed(): void
    {
        $this->trashed = $this->model->onlyTrashed()->count();
    }

    private function setCollectionView(): void
    {
        $pages = $this->getPagesToIndex();
        $trashed = $this->trashed;

        $this->collectionView = compact('pages', 'trashed');
    }

    private function getPagesToIndex(){
        return $this->pages->map(function($page){
            return $page->fields->whereIn('name', ['logo_color', 'title'])
                ->keyBy('name')
                ->map(function($field){
                    if($field->name == 'logo_color' && !stristr($field->value, '#'))
                        $field->value = '#'.$field->value;
                    return $field->value;
                })
                ->merge([
                    'slug' => $page->slug,
                    'date' => $page->created_at->format('d.m.Y'),
                    'parent_slug' => $page->parent->slug
                ]);
        });
    }
}
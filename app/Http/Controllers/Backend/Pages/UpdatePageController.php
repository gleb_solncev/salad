<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 22:53
 */


namespace App\Http\Controllers\Backend\Pages;


use App\Models\Page;
use Illuminate\Http\Request;

class UpdatePageController extends AbstractPageController implements iPageResource
{
    private $page;

    private $request;


    public function __construct(Request $request,Page $page)
    {
        parent::__construct();

        $this->page = $page;
        $this->request = $request;
    }

    public function start()
    {
        $this->updatePage();
        $this->updateFields();

        return redirect()->back()
            ->with('info', "Обновление полей произошло - успешно.");
    }

    public function updatePage()
    {
        $this->page->slug = $this->request->slug;
        $this->page->save();
    }

    public function updateFields()
    {
        $fields = collect($this->request->except(['_method', '_token', 'slug']));
        $p_fields = $this->page->fields;
        $fields->map(function($value, $name) use($p_fields){
            $field = $p_fields->where('name', $name)->first();
            if(!$field) return null;
            $field->value = $value;
            $field->save();
        });

                    $this->createnewCSS($fields->get('logo_color'));

    }
}
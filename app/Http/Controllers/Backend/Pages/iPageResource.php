<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 22:42
 */


namespace App\Http\Controllers\Backend\Pages;


interface iPageResource
{
    public function start();
}
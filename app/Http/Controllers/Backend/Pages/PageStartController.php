<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 22:42
 */


namespace App\Http\Controllers\Backend\Pages;


class PageStartController
{
    public static function open(iPageResource $iPage)
    {
        return $iPage->start();
    }
}
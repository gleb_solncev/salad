<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 22:50
 */


namespace App\Http\Controllers\Backend\Pages;


use App\Models\Page;

class EditPageController extends AbstractPageController implements iPageResource
{
    private $page;

    private $collectionView;

    public function __construct(Page $page)
    {
        parent::__construct();

        $this->page = $page;

        $this->setCollection();
    }

    public function start()
    {
        return view('admin.pages.edit', $this->collectionView);
    }

    private function setCollection(){
        $page = $this->page;
        $fields = $page->fields;

        $this->collectionView = compact('page', 'fields');
    }
}
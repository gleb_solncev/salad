<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 22:43
 */


namespace App\Http\Controllers\Backend\Pages;



use Illuminate\Support\Collection;

class IndexPageController extends AbstractPageController implements iPageResource
{
    /**
     * @var Collection $pages
     */
    private $pages;

    private $collectionView;

    public function __construct()
    {
        parent::__construct();

        $this->setPages();
        $this->setCollection();
    }

    public function start()
    {
        return view('admin.pages.index', $this->collectionView);
    }

    private function setPages(): void
    {
        $parent_id = null;
        $type = 'page';
        $isActive = true;

        $this->pages = $this->model
            ->where(compact('parent_id', 'type', 'isActive'))
            ->get();
    }

    public function setCollection()
    {
        // Заголовок дата слаг цвет
        $pages = $this->pages
            ->map(function($page){
                $collection = $page->fields
                    ->whereIn('name', ['logo_color', 'title'])->keyBy('name')
                    ->map(function($field){
                        return $field->value;
                    });
                $collection['slug'] = $page->slug;
                $collection['date'] = $page->created_at->format('d.m.Y');

                return $collection;
            })->map(function($item){
                if(!stristr($item['logo_color'], '#'))
                    $item['logo_color'] = '#'.$item['logo_color'];

                return $item;
            });

        $this->collectionView = compact('pages');
    }
}
<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 24.01.2020 22:44
 */


namespace App\Http\Controllers\Backend\Pages;


use App\Http\Controllers\Backend\BaseController;
use App\Models\Page;
use App\Services\PageFields\ServicePageFields;

abstract class AbstractPageController extends BaseController
{

    /**
     * @var Page $model
     */
    protected $model;

    /**
     * @var ServicePageFields $service
     */
    protected $service;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Page;
        $this->service = new ServicePageFields;
    }
}
<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Pages\EditPageController;
use App\Http\Controllers\Backend\Pages\IndexPageController;
use App\Http\Controllers\Backend\Pages\PageStartController;
use App\Http\Controllers\Backend\Pages\UpdatePageController;
use App\Services\PageFields\ServicePageFields;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    /**
     * @var Page $model
     */
    private $model;

    /**
     * @var ServicePageFields $service
     */
    private $service;

    /**
     * PageController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model = new Page;
        $this->service = new ServicePageFields;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PageStartController::open(new IndexPageController);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return PageStartController::open(new EditPageController($page));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        return PageStartController::open(new UpdatePageController($request, $page));
    }

}

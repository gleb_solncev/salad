<?php

namespace App\Http\Controllers\Backend;

use App\Models\Message;

class HomeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $messages = Message::orderBy('created_at', 'DESC')->paginate(5);

        return view('admin.home',
            compact('messages')
        );
    }
}

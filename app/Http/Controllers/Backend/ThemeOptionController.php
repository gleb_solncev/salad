<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\ThemeOptions\{
    IndexThemeOptionController,
    OpenEntityThemeOption,
    UpdateThemeOptionController
};
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class ThemeOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Page $page)
    {
        return OpenEntityThemeOption::open(new IndexThemeOptionController($page));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Page $page, Request $request, $id)
    {
        return OpenEntityThemeOption::open(new UpdateThemeOptionController($page, $request));
    }

}

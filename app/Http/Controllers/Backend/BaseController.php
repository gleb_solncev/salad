<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 03.01.2020 11:03
 */


namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;

abstract class BaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
}
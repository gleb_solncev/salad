<?php

namespace App\Http\Requests;

use App\Models\Page;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $name = $this->getNames();
//        $rules =
//            $name
//            ->map(function($name){
//                return [$name => 'required|max:255'];
//            })
//            ->collapse(1)
//            ->merge(['slug' => 'required|unique:pages,slug|max:255|min:2'])
//            ->toArray();

//        return [];
        return ['slug' => 'required|unique:pages,slug|max:255|min:2'];
    }

    /**
     * Получить сообщения об ошибках для определённых правил проверки.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'slug.required' => 'Поле SLUG должно быть заполнено!',
            'slug.unique'  => 'Поле SLUG такое уже сущестует. Пожалуйста измените текст',
        ];
    }
}

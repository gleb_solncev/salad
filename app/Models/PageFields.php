<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PageFields extends Model
{

    protected $fillable = [
        'lang', 'page_id', 'title',
        'type', 'name', 'value'
    ];
    protected $table = 'page_fields';

    public function image()
    {
        return $this->belongsTo('App\Models\Media', 'value', 'id');
    }
}

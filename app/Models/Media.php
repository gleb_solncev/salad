<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table='media',
        $fillable = ['link', 'storage_path', 'size', 'filename', 'type'];
}

<?php

namespace App\Models;

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    protected $table = 'pages';

    protected $fillable = ['slug', 'parent_id'];


    /**
     * Поиск по SLUG
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    /**
     * Полечение полей <Один к многим>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fields()
    {
        $lang = LocaleMiddleware::getLocale() ?: LocaleMiddleware::$mainLanguage;

        return $this
            ->hasMany('App\Models\PageFields', 'page_id', 'id')
            ->where('lang', $lang);
    }

    /**
     * Получение страницы родителя (Для Single, from Post Type) <Один к Одному>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Page', 'parent_id', 'id');
    }

    /**
     * * Получение страницы дети (Для Post Type) <Один кo Многим>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany('App\Models\Page', 'parent_id', 'id');
    }


}

<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 23.01.2020 15:20
 */


namespace App\Services\TypeViewPostType;


use Illuminate\Support\Facades\Storage;

class PageViews implements iTypeView
{
    private $path = 'frontend/pages/';

    public function getViews()
    {
        return Storage::disk('views')->files($this->path);
    }

    public function getPath()
    {
        return $this->path;
    }
}
<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 23.01.2020 15:16
 */


namespace App\Services\TypeViewPostType;


interface iTypeView
{
    public function getViews();
    public function getPath();
}
<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 23.01.2020 15:16
 */


namespace App\Services\TypeViewPostType;


use Illuminate\Support\Facades\Storage;

class SingleViews implements iTypeView
{
    private $path = 'frontend/single/';

    public function getViews()
    {
        return Storage::disk('views')->files($this->path);
    }

    public function getPath()
    {
        return $this->path;
    }
}
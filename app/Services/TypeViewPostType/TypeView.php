<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 23.01.2020 17:32
 */


namespace App\Services\TypeViewPostType;


class TypeView
{
    public static function getView(iTypeView $type, $slug)
    {
        $data = $type->getViews();

        $view = collect($data)
            ->map(function($item) use($slug){
                $path = str_replace('.blade.php', '', $item);
                $ex = explode('/', $path);
                $filename = end($ex);
                return ($filename == $slug)?$path:null;
            })
            ->filter()->first();

        return ($view)?str_replace('/', '.', $view):abort(404);
    }
}
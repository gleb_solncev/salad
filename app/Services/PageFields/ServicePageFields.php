<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 09.01.2020 12:05
 *
 * Поля для страни
 * Здесь идет получение всех полей как для заданой страницы, так и для коллекции
 */


namespace App\Services\PageFields;


use App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class ServicePageFields
{
    private $status;

    protected $fields, $slug;

    /**
     * @param string $slug
     * @param Collection $collection
     */
    private function checkerStatus(): void
    {
        $this->status = false;
        if (!empty($this->slug)) $this->status = true;

        if ($this->fields->isEmpty()) $this->status = true;
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    public function collectionPages(Collection $collection)
    {
        return $collection->map(function ($item) {
            return $this->getFieldsByPage($item);
        });
    }

    /**
     * @param Model $page
     * @return mixed
     */
    public function getFieldsByPage(Page $page)
    {
        $collection = collect();
        $this->setSlug($page->slug);
        $this->setFields($page->fields);

        $this->checkerStatus();
        if (!$this->status or !$this->fields) return $collection;

        $collection = $this->fields
            ->reject(function($item){
                return empty($item['name']);
            })->keyBy('name')
            ->map(function ($item) {
                return ($item->type == "media")?$item:$item->value;
            });

        if($collection->isEmpty()) return $collection;
        if($this->slug) $collection = $collection->merge(['slug' => $this->slug]);

        return $collection;
    }

    public function getFieldsByCollection(Collection $collection)
    {
        return $collection->map(function($page){
            $fields = $this->getFieldsByPage($page);
            return $fields->isNotEmpty()?$fields:null;
        })->filter();
    }

    protected function setFields($fields)
    {
        $this->fields = $fields;
    }

    protected function setSlug($slug){
        $this->slug = $slug;
    }

}
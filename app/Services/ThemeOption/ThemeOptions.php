<?php
/**
 * salad
 * Created by: 5-HT.
 * Date: 28.01.2020 16:10
 */


namespace App\Services\ThemeOption;


use App\Models\Page;

class ThemeOptions
{
    public static function settings()
    {
        $page = Page::where('type', 'theme_option')
            ->where('slug', 'settings')->first();

        return $page->fields->keyBy('name')
            ->whereIn('name', ['settings_email','settings_instagram_link','settings_facebook_link'])
            ->map(function($field){
                return $field->value;
            });
    }
}
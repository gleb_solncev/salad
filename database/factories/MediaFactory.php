<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Media;
use Faker\Generator as Faker;

$factory->define(Media::class, function (Faker $faker) {

    $json = file_get_contents('https://api.giphy.com/v1/gifs/random?api_key=n7AENBIhk8TYBMMjy10LcTPKhZOTQ5ea&tag=fail');
    $data = collect(json_decode($json, true))->get('data');
    $url = collect($data)->get('image_original_url');
    $filename = $faker->uuid.".gif";

    $gif = file_get_contents($url);
    $storage_path = storage_path('app\\public\\'.$filename);
    file_put_contents($storage_path, $gif,FILE_APPEND | LOCK_EX);
    $size = File::size($storage_path);

    print $storage_path." - OK! [$size]\n";
    shell_exec('cls');

    return [
        'link' => $url,
        'storage_path' => $storage_path,
        'size' => $size,
        'filename' => $filename,
        'type' => 'image'
    ];
});

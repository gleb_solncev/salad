<?php

use App\Models\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageFieldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # POST TITLE
        $data = Page::all()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++)
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Название вкладки',
                    'type' => 'text',
                    'name' => 'post_title',
                    'value' => 'SALAD - ' . $page->slug . "[" . $langs[$i] . "]",
                    'created_at' => now()
                ];
            return $data_with_lang;
        })->flatten(1)->toArray();

        DB::table('page_fields')->insert($data);

        # TITLE
        $data = Page::all()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++)
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок',
                    'type' => 'text',
                    'name' => 'title',
                    'value' => $page->slug . "[" . $langs[$i] . "]",
                    'created_at' => now()
                ];
            return $data_with_lang;
        })->flatten(1)->toArray();

        DB::table('page_fields')->insert($data);


        # COLOR LOGO
        $data = Page::all()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];
            if($page->slug != 'settings') {
                for ($i = 0; $i < 3; $i++)
                    $data_with_lang[] = [
                        'page_id' => $page->id,
                        'lang' => $langs[$i],
                        'title' => 'Цвет логотипа',
                        'type' => 'color',
                        'name' => 'logo_color',
                        'value' => '227b14',
                        'created_at' => now()
                    ];
                return $data_with_lang;
            }
        })->flatten(1)->filter()->toArray();

        DB::table('page_fields')->insert($data);


        # Image To Page
//        $data = Page::all()->map(function($page){
//            $langs = ['ru', 'en', 'uk'];
//
//            for($i=0;$i<3;$i++)
//                $data_with_lang[] = [
//                    'page_id' => $page->id,
//                    'lang' => $langs[$i],
//                    'title' => 'Изображение',
//                    'type' => 'media',
//                    'name' => 'image',
//                    'value' => '',
//                    'created_at' => now()
//                ];
//            return $data_with_lang;
//        })->flatten(1)->toArray();
//
//        DB::table('page_fields')->insert($data);


        # HOME
        $data = Page::where('slug', 'home')->get()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++):
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заглавное видео',
                    'type' => 'media',
                    'name' => 'video',
                    'value' => '1',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => '',
                    'type' => 'text',
                    'name' => 'marquee',
                    'value' => 'Пацанский Флекс',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Видео 1',
                    'type' => 'media',
                    'name' => 'video_1',
                    'value' => '1',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Видео 2',
                    'type' => 'media',
                    'name' => 'video_2',
                    'value' => '1',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Видео 3',
                    'type' => 'media',
                    'name' => 'video_3',
                    'value' => '1',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Видео 4',
                    'type' => 'media',
                    'name' => 'video_4',
                    'value' => '1',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Видео 5',
                    'type' => 'media',
                    'name' => 'video_5',
                    'value' => '1',
                    'created_at' => now()
                ];
            endfor;

            return $data_with_lang;
        })->flatten(1)->toArray();
        DB::table('page_fields')->insert($data);

        # ABOUT
        $data = Page::where('slug', 'about')->get()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++):
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок',
                    'type' => 'text',
                    'name' => 'about__title',
                    'value' => 'О',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок вторая часть',
                    'type' => 'text',
                    'name' => 'bordered',
                    'value' => ' нас',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Описание',
                    'type' => 'textarea',
                    'name' => 'about__desc',
                    'value' => 'делаем серьезный ветер',
                    'created_at' => now()
                ];
            endfor;

            return $data_with_lang;
        })->flatten(1)->toArray();
        DB::table('page_fields')->insert($data);


        # VACANCIEWS
        $data = Page::where('slug', 'vacancies')->get()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++):
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок',
                    'type' => 'text',
                    'name' => 'vacancies__title',
                    'value' => 'Вакансии',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок вторая часть',
                    'type' => 'text',
                    'name' => 'vacancies__bordered',
                    'value' => 'ииснакаВ',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Описание',
                    'type' => 'textarea',
                    'name' => 'vacancies__desc',
                    'value' => 'У нас есть и девочки и мальчики ;)',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Изображение(ТОЛЬКО КАРТИНКА)',
                    'type' => 'media',
                    'name' => 'vacancies__img',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Название вакансии #1',
                    'type' => 'text',
                    'name' => 'vacancies__item_title_1',
                    'value' => 'Дизайнер',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Ссылка на вакансию #1',
                    'type' => 'text',
                    'name' => 'vacancies__item_href_1',
                    'value' => 'https://work.ua',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Название вакансии #2',
                    'type' => 'text',
                    'name' => 'vacancies__item_title_2',
                    'value' => 'СММ',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Ссылка на вакансию #2',
                    'type' => 'text',
                    'name' => 'vacancies__item_href_2',
                    'value' => 'https://work.ua',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Название вакансии #3',
                    'type' => 'text',
                    'name' => 'vacancies__item_title_3',
                    'value' => 'Копирайтер',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Ссылка на вакансию #3',
                    'type' => 'text',
                    'name' => 'vacancies__item_href_3',
                    'value' => 'https://work.ua',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Название вакансии #4',
                    'type' => 'text',
                    'name' => 'vacancies__item_title_4',
                    'value' => 'Менеджер',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Ссылка на вакансию #4',
                    'type' => 'text',
                    'name' => 'vacancies__item_href_4',
                    'value' => 'https://work.ua',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Название вакансии #5',
                    'type' => 'text',
                    'name' => 'vacancies__item_title_5',
                    'value' => 'Копирайтер',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Ссылка на вакансию #5',
                    'type' => 'text',
                    'name' => 'vacancies__item_href_5',
                    'value' => 'https://work.ua',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Название вакансии #6',
                    'type' => 'text',
                    'name' => 'vacancies__item_title_6',
                    'value' => 'Копирайтер',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Ссылка на вакансию #6',
                    'type' => 'text',
                    'name' => 'vacancies__item_href_6',
                    'value' => 'https://work.ua',
                    'created_at' => now()
                ];

            endfor;

            return $data_with_lang;
        })->flatten(1)->toArray();
        DB::table('page_fields')->insert($data);

        # CONTACTS
        $data = Page::where('slug', 'contacts')->get()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++):
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок',
                    'type' => 'text',
                    'name' => 'contacts__title',
                    'value' => 'Контакты',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'изображение (Видео не подойдет)',
                    'type' => 'media',
                    'name' => 'contacts__img',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок вторая часть',
                    'type' => 'text',
                    'name' => 'contacts__bordered',
                    'value' => 'ыткатноК',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Номер телефона №1',
                    'type' => 'text',
                    'name' => 'contacts__phone_1',
                    'value' => '777',
                    'created_at' => now()
                ];
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Номер телефона №2',
                    'type' => 'text',
                    'name' => 'contacts__phone_2',
                    'value' => '666',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Почта №1',
                    'type' => 'text',
                    'name' => 'contacts__email_1',
                    'value' => 'Укр пошта',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Почта №2',
                    'type' => 'text',
                    'name' => 'contacts__email_2',
                    'value' => 'Нова пошта',
                    'created_at' => now()
                ];


                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Бегущая строка',
                    'type' => 'text',
                    'name' => 'contacts__words',
                    'value' => 'Серьезный ветер.',
                    'created_at' => now()
                ];

            endfor;

            return $data_with_lang;
        })->flatten(1)->toArray();
        DB::table('page_fields')->insert($data);

        # PROJECTS
        $data = Page::where('slug', 'projects')->get()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++):
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Бегущая строка',
                    'type' => 'text',
                    'name' => 'projects__words',
                    'value' => 'Флексим',
                    'created_at' => now()
                ];

            endfor;

            return $data_with_lang;
        })->flatten(1)->toArray();
        DB::table('page_fields')->insert($data);


        # PROJECTS
        $data = Page::where('parent_id', 5)->get()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++):
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Видео (ТОЛЬКО ВИДЕО)',
                    'type' => 'media',
                    'name' => 'thumbnail',
                    'value' => '1',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок первая часть',
                    'type' => 'text',
                    'name' => 'project_title_1',
                    'value' => 'Мы не ценим наших клиентов',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок вторая часть',
                    'type' => 'text',
                    'name' => 'project_title_2',
                    'value' => 'и соблюдаем конфиденциаоную информацию проекта о ваших половых органах',
                    'created_at' => now()
                ];


                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Бегущая строка',
                    'type' => 'text',
                    'name' => 'project_words',
                    'value' => 'Пацанский флекс',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Задача 1',
                    'type' => 'text',
                    'name' => 'tasks__item_1',
                    'value' => 'Делаем',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Задача 2',
                    'type' => 'text',
                    'name' => 'tasks__item_2',
                    'value' => 'Серьезный',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Задача 3',
                    'type' => 'text',
                    'name' => 'tasks__item_3',
                    'value' => 'Ветер',
                    'created_at' => now()
                ];


                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Изображение 1',
                    'type' => 'media',
                    'name' => 'image_1',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Изображение 2',
                    'type' => 'media',
                    'name' => 'image_2',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Изображение 3',
                    'type' => 'media',
                    'name' => 'image_3',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок 1',
                    'type' => 'text',
                    'name' => 'single_title',
                    'value' => 'Ветренный',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Заголовок 2',
                    'type' => 'text',
                    'name' => 'title_2',
                    'value' => 'Флекс',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Описание',
                    'type' => 'textarea',
                    'name' => 'single_desc',
                    'value' => 'Описание для серьезного ветра',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Изображение',
                    'type' => 'media',
                    'name' => 'right_image',
                    'value' => '2',
                    'created_at' => now()
                ];
                //////


                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 1',
                    'type' => 'media',
                    'name' => 'bottom_images_1',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 2',
                    'type' => 'media',
                    'name' => 'bottom_images_2',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 3',
                    'type' => 'media',
                    'name' => 'bottom_images_3',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 4',
                    'type' => 'media',
                    'name' => 'bottom_images_4',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 5',
                    'type' => 'media',
                    'name' => 'bottom_images_5',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 6',
                    'type' => 'media',
                    'name' => 'bottom_images_6',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 7',
                    'type' => 'media',
                    'name' => 'bottom_images_7',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 8',
                    'type' => 'media',
                    'name' => 'bottom_images_8',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 9',
                    'type' => 'media',
                    'name' => 'bottom_images_9',
                    'value' => '2',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Нижнее изображение 10',
                    'type' => 'media',
                    'name' => 'bottom_images_10',
                    'value' => '2',
                    'created_at' => now()
                ];



            endfor;

            return $data_with_lang;
        })->flatten(1)->toArray();
        DB::table('page_fields')->insert($data);


        # PROJECTS
        $data = Page::where('slug', 'settings')->get()->map(function ($page) {
            $langs = ['ru', 'en', 'uk'];

            for ($i = 0; $i < 3; $i++):
                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Почта',
                    'type' => 'text',
                    'name' => 'settings_email',
                    'value' => 'WELCOME@DIGITAL.SALAD',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Ссылка на инстаграм',
                    'type' => 'text',
                    'name' => 'settings_instagram_link',
                    'value' => 'http://instagram.com/',
                    'created_at' => now()
                ];

                $data_with_lang[] = [
                    'page_id' => $page->id,
                    'lang' => $langs[$i],
                    'title' => 'Ссылка на фейсбук',
                    'type' => 'text',
                    'name' => 'settings_facebook_link',
                    'value' => 'http://fb.com/',
                    'created_at' => now()
                ];

            endfor;

            return $data_with_lang;
        })->flatten(1)->toArray();
        DB::table('page_fields')->insert($data);

    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
//                'id' => 1,
                'slug' => 'home',
                'type' => 'page',
                'isActive' => 1,
                'parent_id' => null,
                'isHomePage' => true,
                'created_at' => now()
            ],
            [
//                'id' => 2,
                'slug' => 'about',
                'type' => 'page',
                'isActive' => 1,
                'parent_id' => null,
                'isHomePage' => 0,
                'created_at' => now()
            ],
            [
//                'id' => 3,
                'slug' => 'contacts',
                'type' => 'page',
                'isActive' => 1,
                'parent_id' => null,
                'isHomePage' => 0,
                'created_at' => now()
            ],
            [
//                'id' => 4,
                'slug' => 'vacancies',
                'type' => 'page',
                'isActive' => 1,
                'parent_id' => null,
                'isHomePage' => 0,
                'created_at' => now()
            ],
            [
//                'id' => 5,
                'slug' => 'projects',
                'type' => 'post_type',
                'isActive' => 1,
                'parent_id' => null,
                'isHomePage' => 0,
                'created_at' => now()
            ],
            [
//                'id' => 6,
                'slug' => 'qq-epta',
                'type' => 'page',
                'parent_id' => 5,
                'isHomePage' => 0,
                'isActive' => 1,
                'created_at' => now()
            ],
            [
//                'id' => 7,
                'slug' => 'settings',
                'type' => 'theme_option',
                'parent_id' => null,
                'isHomePage' => null,
                'isActive' => 1,
                'created_at' => now()
            ],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            [
                'name' => 'Gleb',
                'phone' => '777',
                'message' => 'Hello World!',
            ]
        ]);
    }
}

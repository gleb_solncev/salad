<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media')->insert([
            [
                'id' => 1,
                'link' => 'http://ss.loc/storage/1gVXZIx3MagYS54GDGdBtdl5p6TCTodhM5CpB9sr.mp4',
                'storage_path' => 'storage/app/public/1gVXZIx3MagYS54GDGdBtdl5p6TCTodhM5CpB9sr.mp4',
                'size' => 11111,
                'filename' => '1gVXZIx3MagYS54GDGdBtdl5p6TCTodhM5CpB9sr.mp4',
                'type' => 'video',
                'created_at' => now()
            ],
            [
                'id' => 2,
                'link' => 'http://ss.loc/storage/4durxDjNvkHMKjwskwlAC4ZQTlAvH6UvAvzYVMA5.gif',
                'storage_path' => 'storage\app\public\4durxDjNvkHMKjwskwlAC4ZQTlAvH6UvAvzYVMA5.gif',
                'size' => 11111,
                'filename' => '4durxDjNvkHMKjwskwlAC4ZQTlAvH6UvAvzYVMA5.gif',
                'type' => 'image',
                'created_at' => now()
            ],
        ]);
    }
}

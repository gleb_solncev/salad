<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        factory(Media::class, 100)->create();
        factory(User::class, 5)->create();

        $user = User::where('id', 1)->first();
        $user->email = 'admin@admin.com';
        $user->save();

        $this->call(PageSeeder::class);
        $this->call(PageFieldsSeeder::class);
        $this->call(MediaSeeder::class);
        $this->call(MessageSeeder::class);

    }
}
